# syntax=docker/dockerfile:1

FROM node:12-alpine

LABEL MAINTAINER="Izaias Nascimento<izaiasenelly@gmail.com>"
LABEL APP_VERSION="1.0.0"

RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000

